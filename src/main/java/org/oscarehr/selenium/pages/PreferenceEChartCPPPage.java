/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.pages;

import static org.junit.Assert.assertTrue;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.oscarehr.selenium.resources.PageUtils;

public class PreferenceEChartCPPPage extends LoadableComponent<PreferenceEChartCPPPage> {
	private static Logger log = Logger.getLogger(PreferenceEChartCPPPage.class);
	private final WebDriver driver;
	private final LoadableComponent<?> parent;
	
	/**
	 * References the driver to be used and the parent page to be inherited from
	 * @param driver The web driver to be used
	 * @param parent The parent page which this page inherits control from
	 */
	public PreferenceEChartCPPPage(WebDriver driver, PreferenceMenuPage parent) {
		this.driver = driver;
		this.parent = parent;
	}
	
	/**
	 * Attempts to change the EChart CPP Preference settings
	 * @return the success for changing the preference for the EChart CPPs
	 */
	public boolean editEChartCPPPreferences() {
		log.info("Running method editEChartCPPPreferences...");
		driver.findElement(By.name("cpp.pref.enable")).click();
		
		Select select = new Select(driver.findElement(By.name("cpp.social_hx.position")));
		select.selectByValue("R2I2");
		driver.findElement(By.name("cpp.social_hx.start_date")).click();
		driver.findElement(By.name("cpp.social_hx.res_date")).click();
		
		select = new Select(driver.findElement(By.name("cpp.medical_hx.position")));
		select.selectByValue("R2I1");
		driver.findElement(By.name("cpp.med_hx.start_date")).click();
		driver.findElement(By.name("cpp.med_hx.res_date")).click();
		driver.findElement(By.name("cpp.med_hx.treatment")).click();
		driver.findElement(By.name("cpp.med_hx.procedure_date")).click();
		
		select = new Select(driver.findElement(By.name("cpp.ongoing_concerns.position")));
		select.selectByValue("R1I1");
		driver.findElement(By.name("cpp.ongoing_concerns.start_date")).click();
		driver.findElement(By.name("cpp.ongoing_concerns.res_date")).click();
		driver.findElement(By.name("cpp.ongoing_concerns.problem_status")).click();
		
		select = new Select(driver.findElement(By.name("cpp.reminders.position")));
		select.selectByValue("R1I2");
		driver.findElement(By.name("cpp.reminders.start_date")).click();
		driver.findElement(By.name("cpp.reminders.res_date")).click();
		
		driver.findElement(By.cssSelector("form input")).click();
		return true;
	}
	
	/**
	 * Attempts to load the page navigating to itself from the parent page.
	 */
	@Override
	protected void load() {
		log.info("loading PreferenceEChartCPPPage...");
		parent.get();
		((PreferenceMenuPage) parent).eChartCPPPage();
	}
	
	/**
	 * Verifies that the page is loaded. Checks for the correct URL then checks for an element on the page.
	 */
	@Override
	protected void isLoaded() throws Error {
		log.info("checking if PreferenceEChartCPPPage is loaded...");
		String url = driver.getCurrentUrl();
		assertTrue("Preference EChart CPP Page failed to load: URL does not match", url.contains("/provider/CppPreferences"));
		PageUtils.waitForElementToLoad(By.name("cpp.reminders.position"), driver);
	}
}
