/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.pages;

import static org.junit.Assert.assertTrue;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.oscarehr.selenium.resources.PageUtils;

public class SecurityLogReportPage extends LoadableComponent<SecurityLogReportPage> {
	private static Logger log = Logger.getLogger(SecurityLogReportPage.class);
	private final WebDriver driver;
	private final LoadableComponent<?> parent;
	
	/**
	 * References the driver to be used and the parent page to be inherited from
	 * @param driver The web driver to be used
	 * @param parent The parent page which this page inherits control from
	 */
	public SecurityLogReportPage(WebDriver driver, AdminMenuPage parent) {
		this.driver = driver;
		this.parent = parent;
	}
	
	/**
	 * Checks if the page is printable
	 */
	public boolean createDaySecurityLog() {
		driver.findElement(By.id("startDate_cal")).click();
		driver.findElement(By.cssSelector("div.calendar td.today")).click();
		
		driver.findElement(By.id("endDate_cal")).click();
		driver.findElement(By.cssSelector("div.calendar td.today")).click();
		
		driver.findElement(By.name("submit")).click();
		return true;
	}
	
	/**
	 * Attempts to load the page navigating to itself from the parent page.
	 */
	@Override
	protected void load() {
		log.info("loading SecurityLogReportPage...");
		parent.get();
		((AdminMenuPage)parent).securityLogReportPage();
	}
	
	/**
	 * Verifies that the page is loaded. Checks for the correct URL then checks for an element on the page.
	 */
	@Override
	protected void isLoaded() throws Error {
		log.info("checking if SecurityLogReportPage is loaded...");
		String url = driver.getCurrentUrl();
		assertTrue("Security Log Report Page failed to load: URL does not match", url.contains("/admin/logReport"));
		PageUtils.waitForElementToLoad(By.name("submit"), driver);
	}
}