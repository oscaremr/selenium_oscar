package org.oscarehr.selenium.pages;

import static org.junit.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.oscarehr.selenium.resources.Config;
import org.oscarehr.selenium.resources.PageUtils;
import org.oscarehr.selenium.resources.ScheduleSettings;

/**
 * The Template Setting Page. Appointment Access -> Schedule Setting -> Template Setting
 * The associated JSP page is /schedule/scheduleedittemplate.jsp
 */
public class TemplateSettingPage extends LoadableComponent<TemplateSettingPage> {
	private static Logger log = Logger.getLogger(TemplateSettingPage.class);
	private final WebDriver driver;
	private final LoadableComponent<?> parent;
	
	/**
	 * References the driver to be used and the parent page to be inherited from
	 * @param driver The web driver to be used
	 * @param parent The parent page which this page inherits control from
	 */
	public TemplateSettingPage(WebDriver driver, ScheduleSettingMenuPage parent) {
		this.driver = driver;
		this.parent = parent;
	}
	
	/**
	 * Attempts to create a user schedule template with the associated schedule settings
	 * @param scheduleSettings The schedule settings
	 * @return The success of the creation
	 */
	public boolean createUserTemplateSetting(ScheduleSettings scheduleSettings) {
		driver.findElement(By.name("name")).sendKeys(scheduleSettings.getName());
		driver.findElement(By.name("summary")).sendKeys(scheduleSettings.getSummary());
		
		for(int i=36; i<71; i++) {
			driver.findElement(By.name("timecode" + i)).sendKeys(scheduleSettings.getTimeCode());
		}
		
		//TODO: Change this to a name/id selector once one has been put in
		driver.findElement(By.xpath("/html/body/table/tbody/tr/td[2]/form[3]/table[2]/tbody/tr/td[2]/input[4]")).click();
		return true;
	}
	
	/**
	 * Attempts to load the page navigating to itself from the parent page.
	 */
	@Override
	protected void load() {
		log.info("loading TemplateSettingPage...");
		parent.get();
		((ScheduleSettingMenuPage)parent).templateSettingPage(Config.getScheduleSettings().getProviderId());
	}
	
	/**
	 * Verifies that the page is loaded. Checks for the correct URL then checks for an element on the page.
	 */
	@Override
	protected void isLoaded() throws Error {
		log.info("checking if TemplateSettingPage is loaded...");
	    String url = driver.getCurrentUrl();
	    assertTrue("Schedule Setting Home Page failed to load: URL does not match", url.contains("/schedule/scheduleedittemplate"));
	    PageUtils.waitForElementToLoad(By.name("summary"), driver);
	}
}
