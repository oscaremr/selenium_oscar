/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.*;
import org.oscarehr.selenium.resources.Config;
import org.oscarehr.selenium.resources.PageUtils;

import static org.junit.Assert.assertTrue;

/**
 * The Appointment Access Page. Login -> Appointment Access
 * The associated JSP page is /provider/providercontrol.jsp
 */
public class AppointmentAccessPage extends LoadableComponent<AppointmentAccessPage> {
	private static Logger log = Logger.getLogger(AppointmentAccessPage.class);
	private final WebDriver driver;
	private final LoadableComponent<?> parent;
	
	/**
	 * References the driver to be used and the parent page to be inherited from
	 * @param driver The web driver to be used
	 * @param parent The parent page which this page inherits control from
	 */
	public AppointmentAccessPage(WebDriver driver, LoginPage parent) {
		this.driver = driver;
		this.parent = parent;
	}
	
	/**
	 * Navigates to the Appointment Creation Page
	 * @return page loaded
	 */
	public boolean appointmentCreationPage() {
		log.info("Running method appointmentCreationPage...");
		driver.findElement(By.cssSelector("li#today a")).click();
		
		PageUtils.waitForElementToLoad(By.partialLinkText("09:00"), driver);
		driver.findElement(By.partialLinkText("09:00")).click();
		
		PageUtils.switchWindows("ADD APPOINTMENT", driver);
	    String url = driver.getCurrentUrl();
	    return (url.contains("appointment/addappointment"));
	}
	
	/**
	 * Navigates to the Appointment Management Page
	 * @return page loaded
	 */
	public boolean appointmentManagementPage(String lastName) {
		log.info("Running method appointmentManagementPage...");
		driver.findElement(By.cssSelector("li#today a")).click();
		
		PageUtils.waitForElementToLoad(By.partialLinkText(lastName), driver);
		driver.findElement(By.partialLinkText(lastName)).click();
		
		PageUtils.switchWindows("EDIT APPOINTMENT", driver);
	    String url = driver.getCurrentUrl();
	    return (url.contains("appointment/appointmentcontrol"));
	}
	
	/**
	 * Navigates to the Patient Search Page
	 * @return page loaded
	 */
	public boolean patientSearchPage() {
		log.info("Running method patientSearchPage...");
		driver.findElement(By.cssSelector("li#search a")).click();
		
		PageUtils.switchWindows("PATIENT SEARCH", driver);
		String url = driver.getCurrentUrl();
	    return (url.contains("demographic/search"));
	}
	
	/**
	 * Navigates to the Consult Page
	 * @return page loaded
	 */
	public boolean consultPage() {
		log.info("Running method consultPage...");
		driver.findElement(By.cssSelector("li#con a")).click();
		
		PageUtils.switchWindows("Consultation", driver);
		String url = driver.getCurrentUrl();
	    return (url.contains("oscarEncounter/IncomingConsultation"));
	}
	
	/**
	 * Navigates to the Admin Menu Page
	 * @return page loaded
	 */
	public boolean adminPage() {
		log.info("Running method adminPage...");
		driver.findElement(By.cssSelector("li#admin a")).click();
		
		PageUtils.switchWindows("ADMIN PAGE", driver);
		String url = driver.getCurrentUrl();
		return (url.contains("admin/admin"));
	}
	
	/**
	 * Navigates to the Appointment Search Page
	 * @return page loaded
	 */
	public boolean appointmentSearchPage() {
		log.info("Running method appointmentSearchPage...");
		driver.findElement(By.name("searchview")).click();
		
		PageUtils.switchWindows("SEARCH RESULTS", driver);
		String url = driver.getCurrentUrl();
		return (url.contains("appointment/appointmentsearch"));
	}
	
	/**
	 * Navigates to the Tickler Page
	 * @return page loaded
	 */
	public boolean ticklerPage() {
		log.info("Running method ticklerPage...");
		driver.findElement(By.partialLinkText("ckler")).click();
		
		PageUtils.switchWindows("Tickler", driver);
		String url = driver.getCurrentUrl();
		return (url.contains("tickler/ticklerMain"));
	}
	
	/**
	 * Navigates to the Preference Page
	 * @return page loaded
	 */
	public boolean preferencePage() {
		log.info("Running method preferencePage...");
		driver.findElement(By.partialLinkText("ref")).click();
		
		PageUtils.switchWindows("PREFERENCES", driver);
		String url = driver.getCurrentUrl();
		return (url.contains("provider/providerpreference"));
	}
	
	/**
	 * Selects a pre-existing group and checks for an existing member
	 * @return the existence of a member that is in the group
	 */
	public boolean groupAppointmentView() {
		log.info("Running method groupAppointmentView...");
		Select select = new Select(driver.findElement(By.id("mygroup_no")));
		select.selectByVisibleText("0000");
		
		return (driver.findElement(By.partialLinkText("system")) != null);
	}
	
	/**
	 * Attempts to load the page navigating to itself from the parent page.
	 */
	@Override
	protected void load() {
		log.info("loading AppointmentAccessPage...");
		parent.get();
		((LoginPage) parent).login(Config.getGoodCredentials());
	}
	
	/**
	 * Verifies that the page is loaded. Checks for the correct URL then checks for an element on the page.
	 */
	@Override
	protected void isLoaded() throws Error {
		log.info("checking if AppointmentAccessPage is loaded...");
		String url = driver.getCurrentUrl();
		assertTrue("Appointment Access Page failed to load: URL does not match", url.contains("/provider/providercontrol"));
		PageUtils.waitForElementToLoad(By.cssSelector("li#admin a"), driver);
	}
}
