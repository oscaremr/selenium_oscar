/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.pages;

import static org.junit.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.oscarehr.selenium.resources.Config;
import org.oscarehr.selenium.resources.PageUtils;
import org.oscarehr.selenium.resources.ProviderDetails;

/**
 * The Provider Edit Page. Appointment Access -> Admin -> Search Provider Record -> Edit Provider Record
 * The associated JSP page is /admin/providerupdateprovider.jsp
 */
public class ProviderEditPage extends LoadableComponent<ProviderEditPage> {
	private static Logger log = Logger.getLogger(ProviderEditPage.class);
	private final WebDriver driver;
	private final LoadableComponent<?> parent;
	
	/**
	 * References the driver to be used and the parent page to be inherited from
	 * @param driver The web driver to be used
	 * @param parent The parent page which this page inherits control from
	 */
	public ProviderEditPage(WebDriver driver, ProviderSearchPage parent) {
		this.driver = driver;
		this.parent = parent;
	}
	
	/**
	 * Attempts to edit the existing provider record with the associated provider details
	 * @param providerDetails The provider details
	 * @return The success of the modification
	 */
	public boolean editProviderRecord(ProviderDetails providerDetails) {
		log.info("Running method editProviderRecord...");
		driver.findElement(By.name("sex")).sendKeys(providerDetails.getSex());
		driver.findElement(By.name("dob")).sendKeys(providerDetails.getDateOfBirth());
		driver.findElement(By.name("address")).sendKeys(providerDetails.getAddress());
		driver.findElement(By.name("phone")).sendKeys(providerDetails.getPhone());
		driver.findElement(By.name("workphone")).sendKeys(providerDetails.getWorkphone());
		driver.findElement(By.name("email")).sendKeys(providerDetails.getEmail());
		
		driver.findElement(By.name("subbutton")).submit();
		return true;
	}
	
	/**
	 * Attempts to load the page navigating to itself from the parent page.
	 */
	@Override
	protected void load() {
		log.info("loading ProviderEditPage...");
		parent.get();
		((ProviderSearchPage)parent).searchProviderRecord(Config.getProviderOneDetails().getLastName());
	}
	
	/**
	 * Verifies that the page is loaded. Checks for the correct URL then checks for an element on the page.
	 */
	@Override
	protected void isLoaded() throws Error {
		log.info("checking if ProviderEditPage is loaded...");
	    String url = driver.getCurrentUrl();
	    assertTrue("Provider Edit Page failed to load: URL does not match", url.contains("/admin/providerupdateprovider"));
	    PageUtils.waitForElementToLoad(By.name("subbutton"), driver);
	}
}
