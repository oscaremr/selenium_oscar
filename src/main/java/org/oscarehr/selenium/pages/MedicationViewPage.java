/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.selenium.pages;

import static org.junit.Assert.assertTrue;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.oscarehr.selenium.resources.PageUtils;

public class MedicationViewPage extends LoadableComponent<MedicationViewPage> {
	private static Logger log = Logger.getLogger(MedicationViewPage.class);
	private final WebDriver driver;
	private final LoadableComponent<?> parent;
	
	/**
	 * References the driver to be used and the parent page to be inherited from
	 * @param driver The web driver to be used
	 * @param parent The parent page which this page inherits control from
	 */
	public MedicationViewPage(WebDriver driver, PatientRxPage parent) {
		this.driver = driver;
		this.parent = parent;
	}
	
	/**
	 * Attempts to add a medication to the favorite medications list
	 * @return successful addition of medication to favorites list
	 */
	public boolean addFavoriteMedication() {
		log.info("running method addFavoriteMedication...");
		driver.findElement(By.xpath("/html/body/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td/table/tbody/tr[2]/td[7]/input[2]")).click();
		
		PageUtils.confirmAlertWindow(driver);
		
		PageUtils.switchWindows("Search For Drug", driver);
		return (driver.findElement(By.partialLinkText("AZARGA")) != null);
	}
	
	/**
	 * Attempts to load the page navigating to itself from the parent page.
	 */
	@Override
	protected void load() {
		log.info("loading MedicationViewPage...");
		PageUtils.switchWindows("Search for Drug", driver);
		parent.get();
		((PatientRxPage)parent).viewMedicationPage();
	}
	
	/**
	 * Verifies that the page is loaded. Checks for the correct URL then checks for an element on the page.
	 */
	@Override
	protected void isLoaded() throws Error {
		log.info("checking if MedicationViewPage is loaded...");
		String url = driver.getCurrentUrl();
		assertTrue("Medication View Page failed to load: URL does not match", url.contains("/oscar/oscarRx/StaticScript2"));
		PageUtils.waitForElementToLoad(By.className("ControlPushButton"), driver);
	}
}