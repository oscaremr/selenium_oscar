/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.pages;

import static org.junit.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.*;
import org.oscarehr.selenium.resources.Config;
import org.oscarehr.selenium.resources.PageUtils;
import org.oscarehr.selenium.resources.RxPrescription;

/**
 * The Patient Rx Page. Appointment Access -> Search -> Patient Rx
 * The associated JSP page is /oscarRx/choosePatient.jsp
 */
public class PatientRxPage extends LoadableComponent<PatientRxPage> {
	private static Logger log = Logger.getLogger(PatientRxPage.class);
	private final WebDriver driver;
	private final LoadableComponent<?> parent;
	
	/**
	 * References the driver to be used and the parent page to be inherited from
	 * @param driver The web driver to be used
	 * @param parent The parent page which this page inherits control from
	 */
	public PatientRxPage(WebDriver driver, PatientSearchPage parent) {
		this.driver = driver;
		this.parent = parent;
	}
	
	/**
	 * Attempt to insert a new prescription with the associated Rx Prescription
	 * @param rxPrescription The Rx Prescription details
	 * @return the success of the addition
	 */
	public boolean addPrescription(RxPrescription rxPrescription) {
		log.info("Running method addPrescription...");
		driver.findElement(By.id("searchString")).sendKeys(rxPrescription.getPrescriptionName());
		driver.findElement(By.name("search")).click();
		
		PageUtils.switchWindows("Drug Search", driver);
		
		driver.findElement(By.partialLinkText(rxPrescription.getPrescriptionName())).click();
		
		PageUtils.switchWindows("Search For Drug", driver);
		
		//Wait for the prescription input to process
		PageUtils.waitForElementToLoad(By.cssSelector("input[id^='instructions_']"), driver);
		driver.findElement(By.cssSelector("input[id^='instructions_']")).sendKeys(rxPrescription.getInstructions());
		driver.findElement(By.cssSelector("input[id^='quantity_']")).click();
		
		//Wait for the javascript functions to process the instructions before verification
		PageUtils.waitForElementToContainValue(By.cssSelector("input[id^='quantity_']"), rxPrescription.getQuantity(), driver);
		driver.findElement(By.cssSelector("input[id^='repeats_']")).getText().equals(rxPrescription.getRepeats());
		driver.findElement(By.cssSelector("a[id^='method_']")).getText().equals(rxPrescription.getMethod());
		driver.findElement(By.cssSelector("a[id^='route_']")).getText().equals(rxPrescription.getRoute());
		driver.findElement(By.cssSelector("a[id^='frequency_']")).getText().equals(rxPrescription.getFrequency());
		driver.findElement(By.cssSelector("a[id^='minimum_']")).getText().equals(rxPrescription.getMinimum());
		driver.findElement(By.cssSelector("a[id^='maximum_']")).getText().equals(rxPrescription.getMaximum());
		driver.findElement(By.cssSelector("a[id^='duration_']")).getText().equals(rxPrescription.getDuration());
		driver.findElement(By.cssSelector("a[id^='durationUnit_']")).getText().equals(rxPrescription.getDurationUnit());
		driver.findElement(By.cssSelector("a[id^='quantityStr_']")).getText().equals(rxPrescription.getQuantityStr());
		
		driver.findElement(By.id("saveOnlyButton")).click();
		
		//driver.switchTo().frame("lightwindow_iframe");
		//PageUtils.waitForElementToLoad(By.cssSelector("textarea#additionalNotes"), driver);
		//driver.findElement(By.cssSelector("textarea#additionalNotes")).sendKeys(rxPrescription.getAdditionalNotes());
		//TODO: the "Add to Rx" button needs to be clicked once a proper id/name is assigned to it the xpath does not work due to selecting frames issue
		//driver.findElement(By.xpath("/html/body/div/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[11]/td/input")).click();
		
		//driver.findElement(By.cssSelector("input.ControlPushButton")).click();
		
		return true;
	}
	
	/**
	 * Attempts to check the Drugref Info page
	 * @return page loaded
	 */
	public boolean drugrefInfo() {
		log.info("running method drugrefInfo...");
		driver.findElement(By.partialLinkText("Drugref Info")).click();
		
		PageUtils.switchWindows("Drugref Info", driver);
		String url = driver.getCurrentUrl();
		return (url.contains("/oscarRx/drugrefInfo"));
	}
	
	/**
	 * Attempts to discontinue a medication
	 * @return successful discontinued a medication
	 */
	public boolean discontinueMedication() {
		log.info("running method discontinueMedication...");
		driver.findElement(By.cssSelector("a[id^='discont']")).click();
		
		Select select = new Select(driver.findElement(By.id("disReason")));
		select.selectByVisibleText("Other");
		
		driver.findElement(By.id("disComment")).sendKeys("This is a test");
		driver.findElement(By.xpath("/html/body/div[3]/input[3]")).click();
		return true;
	}
	
	/**
	 * Attempts to add a custom drug
	 * @return successful addition of a custom drug
	 */
	public boolean addCustomDrug() {
		log.info("running method addCustomDrug...");
		driver.findElement(By.id("customDrug")).click();
		PageUtils.confirmAlertWindow(driver);
		
		driver.findElement(By.cssSelector("input[id^='drugName']")).sendKeys("Canesten Cream with 1⁄2% Hydrocortisone powder");
		driver.findElement(By.cssSelector("input[id^='instructions']")).sendKeys("3 WEEKS TOP BID Apply 30 MG");
		
		driver.findElement(By.id("saveOnlyButton")).click();
		return true;
	}
	
	/**
	 * Navigates to the View Medication Page
	 * @return page loaded
	 */
	public boolean viewMedicationPage() {
		log.info("running method viewMedicationPage...");
		driver.findElement(By.partialLinkText("Canesten Cream")).click();
		
		PageUtils.switchWindows("View Medication", driver);
		String url = driver.getCurrentUrl();
		return (url.contains("/oscar/oscarRx/StaticScript2"));
	}
	
	/**
	 * Attempts to add a new medication to the patient's favourite medications list
	 * @return successful addition of medication to favourites list
	 */
	public boolean addFavoriteMedication() {
		log.info("Running method addFavoriteMedication...");
		driver.findElement(By.id("searchString")).sendKeys("AZARGA");
		driver.findElement(By.name("search")).click();
		
		PageUtils.switchWindows("Drug Search", driver);
		
		driver.findElement(By.partialLinkText("AZARGA")).click();
		
		PageUtils.switchWindows("Search For Drug", driver);
		
		PageUtils.waitForElementToEnable(By.cssSelector("span[id^='moreLessWord']"), driver);
		driver.findElement(By.cssSelector("span[id^='moreLessWord']")).click();
		PageUtils.waitForElementToLoad(By.partialLinkText("Add To Favorite"), driver);
		driver.findElement(By.partialLinkText("Add to Favorite")).click();
		
		PageUtils.confirmAlertWindow(driver);
		return (driver.findElement(By.partialLinkText("AZARGA")) != null);
	}
	
	/**
	 * Attempts to hide a prescription from the EChart CPP Page
	 * @return successfully hides a prescription from the CPP Page
	 */
	public boolean hideCPPDisplay() {
		log.info("Running method hideCPPDisplay...");
		driver.findElement(By.cssSelector("input[id^='hidecpp']")).click();
		return true;
	}
	
	/**
	 * Attempts to check for drug-to-drug/drug-to-alergy interactions information
	 * @return drug-to-drug/drug-to-alergy interaction information is present
	 */
	public boolean checkDrugInteraction() {
		driver.findElement(By.id("interactionsRxMyD"));
		return true;
	}
	
	/**
	 * Attempts to load the page navigating to itself from the parent page.
	 */
	@Override
	protected void load() {
		log.info("loading PatientRxPage...");
		parent.get();
		((PatientSearchPage)parent).patientRxSearch(Config.getPatientOneDemographicDetails().getLastName());
	}
	
	/**
	 * Verifies that the page is loaded. Checks for the correct URL then checks for an element on the page.
	 */
	@Override
	protected void isLoaded() throws Error {
		log.info("checking if PatientRxPage is loaded...");
		String url = driver.getCurrentUrl();
		assertTrue("Patient Rx Page failed to load: URL does not match", url.contains("/oscarRx/choosePatient"));
		PageUtils.waitForElementToLoad(By.id("searchString"), driver);
	}
}
