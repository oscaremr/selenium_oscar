/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.pages;

import static org.junit.Assert.assertTrue;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.oscarehr.selenium.resources.PageUtils;

public class TicklerCreationPage extends LoadableComponent<TicklerCreationPage> {
	private static Logger log = Logger.getLogger(TicklerCreationPage.class);
	private final WebDriver driver;
	private final LoadableComponent<?> parent;
	
	/**
	 * References the driver to be used and the parent page to be inherited from
	 * @param driver The web driver to be used
	 * @param parent The parent page which this page inherits control from
	 */
	public TicklerCreationPage(WebDriver driver, TicklerMainPage parent) {
		this.driver = driver;
		this.parent = parent;
	}
	
	/**
	 * Attempts to add a tickler to a provider
	 * @return the successful addition of a tickler
	 */
	public boolean addTickler(String lastName) {
		driver.findElement(By.name("Submit")).click();
		
		PageUtils.switchWindows("demographicsearch", driver);
    	PageUtils.waitForElementToLoad(By.name("keyword"), driver);
		
    	driver.findElement(By.name("keyword")).clear();
	    driver.findElement(By.name("keyword")).sendKeys(lastName);
	    driver.findElement(By.name("displaymode")).submit();
	    
	    PageUtils.waitForElementToLoad(By.cssSelector("td.demoId input.mbttn"), driver);
    	driver.findElement(By.cssSelector("td.demoId input.mbttn")).click();

    	PageUtils.switchWindowsByUrl("tickler/ticklerAdd", driver);
    	PageUtils.waitForElementToLoad(By.name("priority"), driver);
    	
	    Select select = new Select(driver.findElement(By.name("priority")));
		select.selectByValue("High");
		
		select = new Select(driver.findElement(By.name("task_assigned_to")));
		//select.selectByVisibleText("doe, doctor");
		select.selectByVisibleText("Test, Doctor");
		
		driver.findElement(By.name("textarea")).sendKeys("this is a test");
		
		driver.findElement(By.xpath("/html/body/table[3]/tbody/tr[6]/td[2]/input")).click();
		return true;
	}
	
	/**
	 * Attempts to load the page navigating to itself from the parent page.
	 */
	@Override
	protected void load() {
		log.info("loading TicklerCreationPage...");
		parent.get();
		((TicklerMainPage) parent).ticklerCreationPage();
	}
	
	/**
	 * Verifies that the page is loaded. Checks for the correct URL then checks for an element on the page.
	 */
	@Override
	protected void isLoaded() throws Error {
		log.info("checking if TicklerCreationPage is loaded...");
		String url = driver.getCurrentUrl();
		assertTrue("Tickler Creation Page failed to load: URL does not match", url.contains("/tickler/ticklerAdd"));
		PageUtils.waitForElementToLoad(By.xpath("/html/body/table[3]/tbody/tr[6]/td[2]/input"), driver);
	}
}
