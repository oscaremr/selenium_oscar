/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.resources;

public class LoginCredentials {
	private String username;
	private String password;
	private String pin;
	
	public LoginCredentials(){ }

	/**
	 * @param username the username for the login credentials
	 * @param password the password for the login credentials
	 * @param pin the pin for the login credentials
	 */
	public LoginCredentials(String username, String password, String pin) {
		this.username = username;
		this.password = password;
		this.pin = pin;
	}

	public void setUsername(String Username){
		this.username = Username;
	}
	
	public String getUsername(){
		return this.username;
	}
	
	public void setPassword(String Password){
		this.password = Password;
	}
	
	public String getPassword(){
		return this.password;
	}	
	
	public void setPin(String Pin){
		this.pin = Pin;
	}
	
	public String getPin(){
		return this.pin;
	}
}
